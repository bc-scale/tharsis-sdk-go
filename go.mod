module gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-sdk-go

go 1.17

require (
	github.com/go-ozzo/ozzo-validation/v4 v4.3.0
	github.com/hashicorp/go-retryablehttp v0.7.1
	github.com/hashicorp/go-slug v0.10.0
	github.com/hasura/go-graphql-client v0.7.2
	github.com/likexian/gokit v0.25.8
	github.com/qiangxue/go-env v1.0.1
	github.com/stretchr/testify v1.8.1
	github.com/zclconf/go-cty v1.10.0
	nhooyr.io/websocket v1.8.7
)

require (
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d // indirect
	github.com/aws/smithy-go v1.13.4 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/go-cmp v0.5.8 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.1 // indirect
	github.com/klauspost/compress v1.15.7 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/objx v0.5.0 // indirect
	golang.org/x/sys v0.0.0-20220627191245-f75cf1eec38b // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/protobuf v1.28.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
