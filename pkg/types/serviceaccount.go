package types

// ServiceAccount provided M2M authentication
type ServiceAccount struct {
	Metadata     ResourceMetadata
	ResourcePath string
	Name         string
	Description  string
}

// The End.
